using reprocessing_message as schema from '../db/shema';

@requires: 'authenticated-user'
service CatalogService {

    entity Interface as projection on schema.Interface;
    entity Log_process as projection on schema.Log_process;
    entity Reprocess as projection on schema.Reprocess;
}