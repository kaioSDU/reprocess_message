namespace reprocessing_message;

entity Interface {
    
    key id                  : UUID;
        id_cpi              : String(100) not null;
        name                : String(255) not null;
        description         : String(255) not null;
        created_at          : DateTime default null;
        updated_at          : DateTime default null;
        status              : Integer;
        fg_reprocess        : Boolean;
        attr_1              : String(100);
        attr_2              : String(100);
        attr_3              : String(100);
        attr_4              : String(100);
        NavLogProcess       : Association[0..*] to Log_process on NavLogProcess.id_interface = $self.id;
}

entity Log_process {
    
    key id              : UUID;    
    id_interface        : String(255) not null;
    id_log_cpi          : String(100) not null;
    origen              : String(100) not null;
    destiny             : String(100) not null;
    group               : String(100) not null;
    header              : String(100) not null;
    payload             : String(100) not null;
    status_cpi          : Integer;
    status_hana         : Integer;    
    type_requisition    : String(100) not null;    
    exec_variable       : String(255);
    created_at          : DateTime default null;
    updated_at          : DateTime default null;
    start_date          : DateTime default null;
    end_date            : DateTime default null;   
    key_1               : String(100);
    key_2               : String(100);
    key_3               : String(100);
    key_4               : String(100);  
    NavInterface        : Association[1] to Interface on NavInterface.id = $self.id_interface;   
    NavReprocess        : Association[1] to Reprocess on NavReprocess.id_log = $self.id;   
}

entity Reprocess {    

    key id      : UUID;    
    id_log      : String(255) not null;
    created_at  : DateTime default null;
    updated_at  : DateTime default null;
    status      : Integer;
    NavLogProcess      : Association[0..*] to Log_process on NavLogProcess.id = $self.id_log;

}